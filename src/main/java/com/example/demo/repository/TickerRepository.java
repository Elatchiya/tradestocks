package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.model.TickerDetails;

@Repository
public interface TickerRepository extends JpaRepository<TickerDetails,Integer> {

}
