package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.TransactionModel;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionModel, Integer> {

//	@Query("SELECT new com.example.demo.model.custom.TotalStocks(t.ticker.id, SUM(t.volume)) "
//			+ "FROM TransactionModel AS t GROUP BY t.ticker.id")
//	List<TotalStocks> getportfolio();

	@Query("select new com.example.demo.model.TransactionModel(t.ticker,sum(t.volume))"
			+ "from TransactionModel as t group by t.ticker.id")
	List<TransactionModel> getportfolio();
}
