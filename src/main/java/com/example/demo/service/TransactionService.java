package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.TransactionModel;
import com.example.demo.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository repo;

	public List<TransactionModel> getAll() {
		return repo.findAll();
	}

	public TransactionModel add(TransactionModel stocks) {
		return repo.save(stocks);
	}

	public int delete(int id) {
		repo.deleteById(id);
		return id;
	}

	public TransactionModel update(TransactionModel stocks) {
		return repo.save(stocks);
	}

	public TransactionModel retrive(int id) {
		return repo.findById(id).get();
	}

	public List<TransactionModel> getportfolio() {
		return repo.getportfolio();
	}

}
