package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.TickerDetails;
import com.example.demo.repository.TickerRepository;

@Service
public class TickerService {
	
	@Autowired
	TickerRepository repo;
	
	public List<TickerDetails> getAll()
	{
		return repo.findAll();
	}
	
	public TickerDetails add(TickerDetails stocks)
	{
		return repo.save(stocks);
	}
	
	public int delete(int id)
	{
		 repo.deleteById(id);
		 return id;
	}
	
	public TickerDetails update(TickerDetails stocks)
	{
		return repo.save(stocks);
	}
	
	
	public TickerDetails retrive(int id)
	{
		return repo.findById(id).get();
	}
	
	

}
