package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "transaction_model")
@Entity
public class TransactionModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Long volume;
	private String transactionType;

	@ManyToOne
	@JoinColumn(name = "TickerId")
	private TickerDetails ticker;

	public TickerDetails getTicker() {
		return ticker;
	}

	public void setTicker(TickerDetails ticker) {
		this.ticker = ticker;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public TransactionModel(int id, Long volume, String transactionType, TickerDetails ticker) {
		super();
		this.id = id;
		this.volume = volume;
		this.transactionType = transactionType;
		this.ticker = ticker;
	}

	public TransactionModel(TickerDetails ticker, Long volume) {
		super();
		this.volume = volume;
		this.ticker = ticker;
	}

	public TransactionModel() {
	}
}
