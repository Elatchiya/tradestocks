package com.example.demo.model.custom;

public class TotalStocks {
	private Integer tickerId;
	private Long totalStocks;

	public TotalStocks(Integer tickerId, Long totalStocks) {
		super();
		this.tickerId = tickerId;
		this.totalStocks = totalStocks;
	}

	public Integer getTickerId() {
		return tickerId;
	}

	public void setTickerId(Integer tickerId) {
		this.tickerId = tickerId;
	}

	public Long getTotalStocks() {
		return totalStocks;
	}

	public void setTotalStocks(Long totalStocks) {
		this.totalStocks = totalStocks;
	}

}
