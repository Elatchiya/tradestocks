package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.model.TransactionModel;
import com.example.demo.service.TransactionService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/stocks/user")
public class UserController {

	@Autowired
	TransactionService transactionService;

	@GetMapping(value = "")
	public List<TransactionModel> getAllStocks() {
		return transactionService.getAll();
	}

	@PostMapping(value = "")
	public TransactionModel addStocks(@RequestBody TransactionModel stocks) {
		if (stocks.getTransactionType().equals("SELL") == true) {
			List<TransactionModel> ts = transactionService.getportfolio();
			for (int i = 0; i < ts.size(); i++) {
				if (ts.get(i).getTicker().getId() == stocks.getTicker().getId()) {
					if (ts.get(i).getVolume() + stocks.getVolume() >= 0) {
						return transactionService.add(stocks);
					}
				}
			}
			throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "NOT POSSIBLE");
		} else {
			return transactionService.add(stocks);
		}
	}

	@PutMapping(value = "")
	public TransactionModel updateStocks(@RequestBody TransactionModel stocks) {
		return transactionService.update(stocks);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStocks(@PathVariable int id) {
		return transactionService.delete(id);
	}

	@GetMapping(value = "/{id}")
	public TransactionModel retriveStocks(@PathVariable("id") int id) {
		return transactionService.retrive(id);
	}

	@GetMapping(value = "/portfolio")
	public List<TransactionModel> getportfolio() {
		return transactionService.getportfolio();
	}
}
