package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.TickerDetails;
import com.example.demo.service.TickerService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/stocks/admin")
public class AdminController {
	@Autowired
	TickerService tickerService;

	@GetMapping(value = "")
	public List<TickerDetails> getAllStocks() {
		return tickerService.getAll();
	}

	@PostMapping(value = "")
	public TickerDetails addStocks(@RequestBody TickerDetails stocks) {
		return tickerService.add(stocks);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStocks(@PathVariable int id) {
		return tickerService.delete(id);
	}

	@PutMapping(value = "")
	public TickerDetails updateStocks(@RequestBody TickerDetails stocks) {
		return tickerService.update(stocks);
	}

	@GetMapping(value = "/{id}")
	public TickerDetails retriveStocks(@PathVariable("id") int id) {
		return tickerService.retrive(id);
	}
}
