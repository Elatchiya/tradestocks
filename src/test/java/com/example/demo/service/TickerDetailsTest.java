package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.model.TickerDetails;
import com.example.demo.repository.TickerRepository;

@SpringBootTest
class TickerDetailsTest {
	@Autowired
	TickerRepository repository;

	@Autowired
	TickerService service;

	@Test
	public void testGetAllUsers() {
		List<TickerDetails> returnedList = service.getAll();

		assertThat(returnedList).isNotEmpty();
		/*
		 * for(TickerDetails user: returnedList) { System.out.println("Username: " +
		 * user.getUsername()); }
		 */
	}

	@Test
	public void testGetUserById() {
		TickerDetails returnedList = repository.findById(3).orElse(new TickerDetails());
		assertThat(returnedList).isNotNull();
	}

	@Test
	public void testCUDPortfolio() {
		TickerDetails user = new TickerDetails(3, "TCS", 100);
		TickerDetails returnedList = service.add(user);
		assertThat(returnedList).isNotNull();
		int id = returnedList.getId();

		TickerDetails updateUser = new TickerDetails(id, "AMZ", 109);
		TickerDetails returnedList1 = service.update(updateUser);
		assertThat(returnedList1).isNotNull();

		int responseId = service.delete(id);
		assertThat(responseId).isEqualTo(id);
	}

}
