package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.model.TransactionModel;
import com.example.demo.repository.TransactionRepository;

;

@SpringBootTest
class TransactionModelTest {

	@Autowired
	TransactionService service;

	@Autowired
	TransactionRepository repository;

	@Autowired
	TickerService ticker_service;

	@Test
	public void testGetAllUsers() {
		List<TransactionModel> returnedList = service.getAll();

		assertThat(returnedList).isNotEmpty();
		/*
		 * for(TransactionModel user: returnedList) { System.out.println("Username: " +
		 * user.getUsername()); }
		 */
	}

	@Test
	public void testGetUserById() {
		TransactionModel returnedList = repository.findById(3).orElse(new TransactionModel());
		assertThat(returnedList).isNotNull();
	}

}
